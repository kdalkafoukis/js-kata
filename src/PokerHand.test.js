import PokerHand, { Result } from './PokerHand.js';

describe('PokerHand', () => {

	describe('compareWith()', () => {

		it(`wins`, () => {

			// const high = new PokerHand('AS 3C 5H 8C 2S');
			// const pair = new PokerHand('AS AC 2H 3C 5S');
			// const two_p = new PokerHand('AS AC 4H 5C 5S');
			// const three_p = new PokerHand('AS AC AH 2C 5S');
			// const straight = new PokerHand('KS QS JS TS 9C');
			// const full_house = new PokerHand('AS AC AH 5C 5S');
			// const four_p = new PokerHand('AS AC AH AD 5S');
			// const straight_flush = new PokerHand('KS QS JS TS 9S');
			// const flush_royal = new PokerHand('AS KS QS JS TS');

			// high
			const hand1 = new PokerHand('AS 3C 5H 8C 2S');
			const hand2 = new PokerHand('KS 3C 5H 8C 2S');

			// const hand1 = new PokerHand('AC KS QS JC TH');
			// const hand2 = new PokerHand('AS 3S 2H 5C 2S');

			expect(hand1.compareWith(hand2)).toBe(Result.WIN);

		});

	});

});
