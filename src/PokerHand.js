const pokerValues = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K','A'];
const pokerSuits = ['S','H','D','C'];
const ranks = [
  'Royal flush',
  'Straight flush',
  'Four of a kind',
  'Full house',
  'Flush',
  'Straight',
  'Three of a kind',
  'Two pairs',
  'Pair',
  'Highcard',
];

export class PokerHand {
	constructor(hand){
		const cards = this.getCards(hand);
    this.card_values = this._getValues(cards);
    this.card_suits = this._getSuits(cards);

		const check = this.checkInvalidInput(cards);
		if(check){
      this.rank = '';
      this.high_card=null;
			this.ranking(cards);
		}
		else{
			console.log('Print out: invalid input');
		}
	}

  getRank(){
    return this.rank;
  }

  getHighCard(){
    return this.high_card;
  }

	getCards(hand){
		return hand.split(' ');
	}

  getValues(){
    return this.card_values;
  }

  getSuits(){
    return this.card_suits;
  }

  _getValues(cards){
    return cards.map(card => pokerValues.indexOf(card[0])).sort((a, b) => a - b);
  }

	_getSuits(cards){
		return cards.map(card => pokerSuits.indexOf(card[1]));
	}

  getSameCards(){
    // it creates an array of maximum two groups of pairs
    const value_cards = this.getValues();
    let straight = true;
    let control=false;
    let array=[[]];
    let index = 0
    for (let i=1;i<value_cards.length;i++){
      if(value_cards[i]===value_cards[i-1]){
        if(control){
          array.push([]);
          index = index+1;
          control = false;
        }
        straight = false;
        array[index].push(value_cards[i]);
      }
      else{
        if(!straight && index<1){
          control = true
        }
      }
      array = array.sort((a, b) => a.length - b.length);
    }
    return array;
  }

	checkInvalidInput(cards){
		let check=true;

		// check if user gave 5 cards
		if (cards.length!==5){
			check = false;
		}
		else{
			// check if the cards are contained to the deck
			cards.forEach(card=>{
				if(!pokerValues.includes(card[0]) || !pokerSuits.includes(card[1])){
					check = false;
				}
			});
		}
		if (check) return true;
		return false;
	}

	ranking(cards){
    const samecards = this.getSameCards();
		this.high_card = this.HighCard();

    const pair = this.Pair(samecards);
    const pairs = this.Pairs(samecards);
    const triple_pair = this.TripplePair(samecards);
    const straight = this.Straight();
		const flush = this.Flush();
    const full_house = this.FullHouse(samecards);
    const for_of_a_kind = this.ForOfAKind(samecards);
    const straight_flush = this.StraightFlush();
    const flush_royal = this.FlushRoyal();

    if(flush_royal) this.rank = ranks[0];
    else if (straight_flush) this.rank = ranks[1];
    else if (for_of_a_kind) this.rank = ranks[2];
    else if (full_house) this.rank = ranks[3];
    else if (flush) this.rank = ranks[4];
    else if (straight) this.rank = ranks[5];
    else if (triple_pair) this.rank = ranks[6];
    else if (pairs) this.rank = ranks[7];
    else if (pair) this.rank = ranks[8];
    else this.rank = ranks[9];

    console.log(this.getRank());

  }

	HighCard(cards){
		const value_cards = this.getValues();
		return value_cards[value_cards.length-1];
	}

  Pair(cards){
    if(cards[0] && !cards[1] && cards[0].length===1) return true;
    return false;
  }

  Pairs(cards){
    if(cards[0] && cards[1] && cards[0].length===cards[1].length) return true;
    return false;
  }

  TripplePair(cards){
    if(cards[0] && !cards[1] && cards[0].length===2) return true;
    return false;
  }

  Straight(){
    const value_cards = this.getValues().reverse();
    let straight = true;
    for (let i=1;i<value_cards.length;i++){
      if(value_cards[i]!==value_cards[i-1]-1){
        straight = false;
      }
    }
    return straight;
  }

  Flush(){
    const suit_cards = this.getSuits();
    let flush = true;
    for (let i=1;i<suit_cards.length;i++){
      if(suit_cards[i]!==suit_cards[i-1]){
        flush = false;
      }
    }
    return flush;
  }

  FullHouse(cards){
    if(cards[0] && cards[1] && cards[0].length===1 && cards[1].length===2) return true;
    return false;
  }

  ForOfAKind(cards){
    if(cards[0] && !cards[1] && cards[0].length===3) return true;
    return false;
  }

  StraightFlush(){
    if (this.Flush() && this.Straight()) return true;
    return false;
  }

  FlushRoyal(){
    const value_cards = this.getValues();
    if (this.StraightFlush() && value_cards.includes(pokerValues.indexOf('A'))) return true;
    return false;
  }

	compareWith(hand2) {
    let rank1 = ranks.indexOf(this.getRank());
    let rank2 = ranks.indexOf(hand2.getRank());
    //if higher card wins
    if(rank1===ranks.length-1 && rank2===ranks.length-1){
      rank1 = this.getHighCard();
      rank2 = hand2.getHighCard();
      // higher rank better
      if(rank1>rank2){
        return Result.WIN;
      }
      else if (rank1<rank2){
        return Result.LOSS;
      }
      else{
        return Result.TIE;
      }
    }

    // lower rank better
    if(rank1<rank2){
      return Result.WIN;
    }
    else if (rank1>rank2){
      return Result.LOSS;
    }
    else{
      return Result.TIE;
    }
	}

}

export const Result = {
	WIN: 1,
	LOSS: 2,
	TIE: 3
};

export default PokerHand;
